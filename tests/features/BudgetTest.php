<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BudgetTest extends TestCase
{
     use DatabaseTransactions;
     /** @test */
     function can_create_a_budget() {
          $user = factory(App\User::class)->create();

          $budget = $user->newBudget('Reiersons', 'weekly');

          $this->seeInDatabase('budgets', [
               'name' => 'Reiersons',
               'type' => 'weekly',
          ]);
     }

     /** @test */
     function can_delete_a_budget() {
          $user = factory(App\User::class)->create();

          $budget = $user->newBudget('Reiersons', 'weekly');

          $this->seeInDatabase('budgets', [
               'name' => 'Reiersons',
               'type' => 'weekly',
          ]);

          $budget->delete(); 
     }

}
