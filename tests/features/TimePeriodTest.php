<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TimePeriodTest extends TestCase
{
     /** @test */
     function can_add_time_period_to_budget() {
          $user = factory(App\User::class)->create();

          $budget = $user->newBudget('Reiersons', 'weekly');

          $timePeriod = $budget->addTimePeriod();

          $this->seeInDatabase('time_periods', [
               'id' => $timePeriod->id,
               'start' => $timePeriod->start,
               'end' => $timePeriod->end,
          ]);
     }
}
