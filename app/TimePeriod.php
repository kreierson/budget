<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class TimePeriod extends Model
{
    protected $fillable = ['start', 'end', 'total_budget'];

    protected $dates = ['start', 'end'];

    public function budgetItems() {
         return $this->hasMany(BudgetItem::class);
    }

    public function transactions()
    {
         return $this->hasMany(Transaction::class)->orderBy('created_at', 'desc');
    }
    
    public function getStartAttribute($value) {
         return Carbon::createFromFormat('Y-m-d H:i:s', $value, Auth::user()->time_zone)->toFormattedDateString();
    }

    public function getEndAttribute($value) {
         return Carbon::createFromFormat('Y-m-d H:i:s', $value, Auth::user()->time_zone)->toFormattedDateString();
    }

}
