<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
     protected $fillable = ['email'];
    public function budget()
    {
         return $this->belongsTo(Budget::class);
    }
}
