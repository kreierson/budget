<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function budgets() {
         return $this->belongsToMany(Budget::class)->orderBy('name');
    }

    public function newBudget($request) {
         $budget = $this->budgets()->create([
               'name' => $request->name,
               'default_day' => $request->start_day,
          ]);

         if($this->budgets->count() == 1) {
               $this->default_budget_id = $budget->id;
               $this->save();
         }
         return $budget;
    }

    public function setDefaultBudget($budget_id)
    {
         $this->default_budget_id = $budget_id;
         $this->save();
    }
}
