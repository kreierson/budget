<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class Budget extends Model
{
    protected $fillable = ['name', 'type', 'default_day'];

    public function users() {
         return $this->belongsToMany(User::class);
    }

    public function transactions()
    {
         return $this->hasMany(Transaction::class);
    }
    
    public function addTimePeriod($request) {
    // dd(strtoupper($start_day));          
          //The date initialization is temporary, we want to figure out if they have any time periods, then default the time period
          //based on the budget. The budget will show which days their budget starts and ends.
          // $start = ($request->start_date == 'prev' ? Carbon::parse('this ' . $request->start_day) : Carbon::parse('next ' . $request->start_day));

          $start = (Carbon::now()->tz(Auth::user()->time_zone)->format('l') == $request->start_day ? Carbon::now() : Carbon::now()->parse('this ' . $request->start_day));
          // dd(Carbon::now()->tz('US/Mountain')->format('l') . ' ' . $request->start_day);
         return $this->timePeriods()->create([
               'start' => ($start == null ? Carbon::now() : $start), 
               'end' => $start->copy()->addDays(7),
          ]);
    }

    public function timePeriods() {
         return $this->hasMany(TimePeriod::class)->orderBy('start', 'desc');
    }

    public function addNewTimePeriod($latestTimePeriod)
    {
          
          $start = Carbon::createFromFormat('M d, Y', $latestTimePeriod->end);
          $newTimePeriod =  $this->timePeriods()->create([
               'start' => ($start == null ? Carbon::now() : $start), 
               'end' => $start->copy()->addDays(7),
          ]);

          foreach($latestTimePeriod->budgetItems as $item) {
               $newTimePeriod->budgetItems()->create([
                    'item_description' => $item->item_description,
                    'amount' => $item->amount,
               ]);
          }
    }

    public function invites()
    {
         return $this->hasMany(Invite::class);
    }
}
