<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['amount', 'budget_id', 'budget_item_id', 'description'];

    public function timePeriod()
    {
         return $this->belongsTo(TimePeriod::class);
    }

    public function budget()
    {
         return $this->belongsTo(Budget::class);
    }

    public function budgetItem()
    {
         return $this->belongsTo(BudgetItem::class);
    }

    public function getAmountAttribute($value)
    {
         return $value;
    }
}
