<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Budget;
use App\TimePeriod;
use Auth;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $budget = Auth::user()->budgets()->where('id', Auth::user()->default_budget_id)->first();
          if(!$budget) {
               return redirect('/budgets');
          }
          $time_periods = TimePeriod::where('budget_id', $budget->id)->orderBy('created_at', 'desc')->paginate(1);
          $data = [
               'time_periods' => $time_periods,
               'budget' => $budget,
          ];
          return view('home', $data);
    }
}
