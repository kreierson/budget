<?php

namespace App\Http\Controllers;

use Auth;
use App\Budget;
use App\TimePeriod;
use Illuminate\Http\Request;
use App\Transaction;

class BudgetController extends Controller
{
    public function index(Request $request) {
          $data = [
               'budgets' => Auth::user()->budgets,
          ];   

          return view('app.budget.index', $data);
    }

    public function show(Request $request) {
         $budget = Budget::with('transactions')->where('id', $request->id)->firstOrFail();
         
         $timePeriods = TimePeriod::with('transactions', 'budgetItems.transactions')->where(['budget_id' => $request->id])->orderBy('start', 'desc')->get();
         $data = [
               'budget' => $budget,
               'timePeriods' => $timePeriods,
         ];
         return view('app.budget.show', $data);
    }

    public function save(Request $request) {
     // dd($request);
          $this->validate($request, [
               'name' => 'required|max:100',
               'start_day' => 'required|in:Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday',
          ]);
         $budget = Auth::user()->newBudget($request);

         if(isset($request->default_budget)) {
               $user = Auth::user();
               $user->setDefaultBudget($budget->id);
         }
         $timePeriod = $budget->addTimePeriod($request);
         return redirect('/budgets/' . $budget->id);
    }

    public function destroy(Request $request) {
         $budget = Budget::where(['id' => $request->id])->firstOrFail();
         if(Auth::user()->default_budget_id == $budget->id && Auth::user()->budgets->count() > 1) {
               return redirect()->back()->withErrors(['You cannot delete a default budget']);
         }
         $budget->delete();

         return redirect()->back();
    }

    public function getTimePeriods(Request $request) {
         return TimePeriod::where('budget_id', $request->id)->get();
    }

    public function addNextTimePeriod(Request $request)
    {
         $budget = Budget::where(['id' => $request->id])->firstOrFail();
         $latestTimePeriod = TimePeriod::where('budget_id', $budget->id)->orderBy('end', 'desc')->firstOrFail();
         // return $latestTimePeriod;
         $budget->addNewTimePeriod($latestTimePeriod);
         return redirect()->back();
         return TimePeriod::with('budgetItems')->where('budget_id', $budget->id)->orderBy('end', 'desc')->firstOrFail();
    }

    public function invite(Request $request)
    {
         //Add email and budget id to invites table.
         $budget = Budget::where('id', $request->id)->firstOrFail();

         $budget->invites()->create([
               'email' => $request->email,
          ]);
         //Email User letting them know they have been invited **Eventually, not now.
         
         return redirect()->back();
    }


    public function deleteTransaction(Request $request)
    {
           $tran = Transaction::where(['id' => $request->id])->firstOrFail();

          $tran->delete();

          return ['status' => 'success'];
   }
    public function makeDefault(Request $request)
    {
          $user = Auth::user();
          $user->setDefaultBudget($request->id);
          return redirect()->back();

    }
}
