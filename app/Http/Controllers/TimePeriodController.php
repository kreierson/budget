<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TimePeriod;
class TimePeriodController extends Controller
{
    public function addBudgetItem(Request $request)
    {
         $time_period = TimePeriod::where('id', $request->id)->firstOrFail();

         return $time_period->budgetItems()->create([
               'item_description' => null,
               'amount' => 0,
          ]);
    }

    public function addTransaction(Request $request)
    {
          $this->validate($request, [
               'amount' => 'required|numeric',
               'budget_item' => 'required|numeric|exists:budget_items,id'
          ]);
         $time_period = TimePeriod::where('id', $request->id)->firstOrFail();

         $time_period->transactions()->create([
               'budget_id' => $time_period->budget_id,
               'budget_item_id' => $request->budget_item,
               'amount' => $request->amount,
               'description' => $request->description,
          ]);

         return redirect()->back();
    }

    public function destroy(Request $request)
    {
         $time_period = TimePeriod::where('id', $request->id)->firstOrFail();
         $time_period->delete();
         return ['status' => 'success'];
    }
}
