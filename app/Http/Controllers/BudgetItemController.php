<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BudgetItem;
use App\TimePeriod;

class BudgetItemController extends Controller
{
    public function destroy(Request $request)
    {
         $item = BudgetItem::where('id', $request->id)->firstOrFail();
         $item->delete();
         return ['status' =>'success'];
    }

    public function saveWeek(Request $request)
    {
          $timePeriod = TimePeriod::where('id', $request->id)->firstOrFail();
         foreach($request->all() as $data) {
               if($data['item_description'] !== null) {
                    if($data['id'] == null) {
                         BudgetItem::create([
                              'time_period_id' => $data['time_period_id'],
                              'item_description' => $data['item_description'],
                              'amount' => $data['amount'],
                         ]);
                    } else {
                         BudgetItem::where('id', $data['id'])->update([
                              'item_description' => $data['item_description'],
                              'amount' => $data['amount'],
                         ]);
                    }
               }
         }
         return $timePeriod->budgetItems;
    }
}
