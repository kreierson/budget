<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetItem extends Model
{
    protected $fillable = ['item_description', 'amount', 'time_period_id'];

    public function timePeriod() {
         return $this->belongsTo(TimePeriod::class);
    }

    public function getAmountAttribute($value) {
         return $value;
    }

    public function transactions()
    {
         return $this->hasMany(Transaction::class);
    }

}
