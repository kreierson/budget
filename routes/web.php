<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/budgets', 'BudgetController@index')->middleware('auth');
Route::post('/budgets/{id}/invite', 'BudgetController@invite')->middleware('auth');
Route::get('/budgets/{id}', 'BudgetController@show')->middleware('auth');
Route::get('/budgets/{id}/time-periods', 'BudgetController@getTimePeriods')->middleware('auth');
Route::post('/budgets/{id}/make-default', 'BudgetController@makeDefault')->middleware('auth');
Route::post('/budgets', 'BudgetController@save')->middleware('auth');
Route::delete('/budgets/{id}', 'BudgetController@destroy')->middleware('auth');
Route::delete('/transaction/{id}', 'BudgetController@deleteTransaction')->middleware('auth');
Route::post('/budgets/{id}/add-period', 'BudgetController@addNextTimePeriod')->middleware('auth');


Route::post('/time-period/{id}/budget-item', 'TimePeriodController@addBudgetItem')->middleware('auth');
Route::delete('/time-period/{id}', 'TimePeriodController@destroy')->middleware('auth');
Route::post('/time-period/{id}/transaction', 'TimePeriodController@addTransaction')->middleware('auth');
Route::post('/budget-item/save-week/{id}', 'BudgetItemController@saveWeek')->middleware('auth');
Route::delete('/budget-item/{id}', 'BudgetItemController@destroy')->middleware('auth');
