@if ($paginator->hasPages())
<nav class="pagination is-center" style="margin-top: 5px;">
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="pagination-previous" href="{{ $paginator->nextPageUrl() }}" rel="next">Previous Week</a>
        @else
           <a class="pagination-previous is-disabled" href="{{ $paginator->nextPageUrl() }}" rel="next">Previous Week</a>
        @endif

         {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
           <a class="pagination-next is-disabled" href="{{ $paginator->previousPageUrl() }}" rel="prev">Next Week</a>
        @else
            <a class="pagination-next" href="{{ $paginator->previousPageUrl() }}" rel="prev">Next Week</a>
        @endif
</nav>
{{--     <nav class="pagination is-right">
     <a class="pagination-previous">Previous</a>
     <a class="pagination-next">Next page</a>
</nav> --}}
@endif
