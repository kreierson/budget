@extends('layouts.app')

@section('content')
<div id="app">
<div class="columns is-multiline">
<div class="column is-8 is-offset-2">
<div class="has-text-centered">

</div>
     @foreach($time_periods as $time_period)
     <div class="has-text-centered">
     <h1 class="title">{{$budget->name}} - {{$budget->type}} budget</h1>
     <h2 class="subtitle">{{$time_period->start}} to {{$time_period->end}}</h2>
     </div>
          {{$time_periods->links('vendor.pagination.bulma')}}
     <hr>
     <form action="/time-period/{{$time_period->id}}/transaction" method="post">
     {{csrf_field()}}
     <p class="control has-addons has-addons-centered">
          <span class="select">
               <select name="budget_item">
                    @foreach($time_period->budgetItems as $item)
                    <option value="{{$item->id}}">{{$item->item_description}}</option>
                    @endforeach
               </select>
          </span>
          <input class="input" name="amount" type="text" placeholder="Amount of money" autofocus>

     </p>
     <div class="column is-6 is-offset-3">
          <p class="control">
               <input class="input" name="description" type="text" placeholder="Description (Optional)">
          </p>
     </div>
     <div class="column has-text-centered">
          <button class="button is-primary is-centered">
               Submit
          </button>
     </div>
</form>
</div>
<div class="column is-8 is-offset-2">
          {{-- Transactions --}}
          <div class="tabs is-toggle is-fullwidth is-large">
               <ul>
                    <li v-bind:class="{'is-active': summaryTabActive}">
                         <a @click="summaryActive">
                              <span class="icon"><i class="fa fa-bar-chart"></i></span>
                              <span>Sumary</span>
                         </a>
                    </li>
                    <li v-bind:class="{'is-active': transactionsTabActive}">
                         <a @click="transactionsActive">
                              <span class="icon"><i class="fa fa-money"></i></span>
                              <span>Transactions</span>
                         </a>
                    </li>
               </ul>
          </div>
          <div class="box" v-show="transactionsTabActive">
                         <div class="is-responsive">
                              <table class="table">
                                   <thead>
                                        <th>Date</th>
                                        <th>Budget Item</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                   </thead>
                                   <tbody>
                                        @foreach($time_period->transactions as $trans)
                                            <tr>
                                                 <td>{{$trans->created_at->toFormattedDateString()}}</td>
                                                 <td>{{$trans->budgetItem->item_description}}</td>
                                                 <td>{{$trans->description}}</td>
                                                 <td>{{$trans->amount}}<span class="pull-right fa fa-times" @click="deleteTransaction({{$trans->id}})"></span></td>
                                            </tr>
                                        @endforeach
                                   </tbody>
                              </table>
                         </div>
          </div>
          <div class="box" v-show="summaryTabActive">
                              <div class="is-responsive">
                              <table class="table">
                                   <thead>
                                        <th>Budget Item</th>
                                        <th>Amount</th>
                                        <th>Spent</th>
                                        <th>Left Over</th>
                                   </thead>
                                   <tbody>
                                        @foreach($time_period->budgetItems as $budget_item)
                                        <tr>
                                             <td>{{$budget_item->item_description}}</td>
                                             <td>{{$budget_item->amount}}</td>
                                             <td>{{$budget_item->transactions->sum('amount')}}</td>
                                             <td v-bind:class="{'is-negative': {{$budget_item->amount - $budget_item->transactions->sum('amount')}} < 0}">{{$budget_item->amount - $budget_item->transactions->sum('amount')}}</td>
                                        </tr>
                                        @endforeach
                                   </tbody>
                                   <tfoot>
                                        <th>Total</th>
                                        <th>${{$time_period->budgetItems->sum('amount')}}</th>
                                        <th>${{$time_period->transactions->sum('amount')}}</th>
                                        <th v-bind:class="{'is-negative': {{$time_period->budgetItems->sum('amount') - $time_period->transactions->sum('amount')}} < 0}">${{$time_period->budgetItems->sum('amount') - $time_period->transactions->sum('amount')}}</th>
                                   </tfoot>
                              </table>
                              </div>
                         
          </div>
     @endforeach
</div>

</div>
</div>
<script>
     new Vue({
          el: '#app',
          data: {
               summaryTabActive: true,
               transactionsTabActive: false,
          },
          methods: {
               summaryActive: function() {
                    this.summaryTabActive = true;
                    this.transactionsTabActive = false;
               },
               transactionsActive: function() {
                    this.summaryTabActive = false;
                    this.transactionsTabActive = true;
               },
               deleteTransaction: function(id) {
                     axios.delete('/transaction/'+id).then(response =>  {
                              window.location.href = '/home';
                         });
               }
          }
     });
</script>
@endsection
