<div class="modal" v-bind:class="{'is-active': modalActive}">
  <div class="modal-background"></div>
  <div class="modal-card">
 <form action="/budgets/{{$budget->id}}/invite" method="post">
 {{csrf_field()}}
    <header class="modal-card-head">
      <p class="modal-card-title">Invite a user to contribute</p>
      <a class="delete" @click="toggleInviteUserModal"></a>
    </header>
    <section class="modal-card-body">
      <label>Email address</label>
     <p class="control">
       <input class="input is-medium" name="email" type="text" placeholder="Enter an email of someone cool">
     </p>
    </section>
    <footer class="modal-card-foot">
      <button class="button is-primary">Invite</button>
      <a class="button" @click="toggleInviteUserModal">Cancel</a>
    </footer>
</form>
  </div>
</div>