    <div id="nav">
<nav class="nav">
  <div class="container">
    <span class="nav-toggle" @click="toggleNav">
      <span></span>
      <span></span>
      <span></span>
    </span>
    <div class="nav-right nav-menu" v-bind:class="{'is-active': navActive}">
    @if (Auth::guest())
        <a href="{{ url('/login') }}" class="{{{ Request::is('login') ? 'nav-item is-tab is-active' : 'nav-item is-tab' }}}">Login</a>
        <a href="{{ url('/register') }}" class="{{{ Request::is('register') ? 'nav-item is-tab is-active' : 'nav-item is-tab' }}}">Register</a>
    @else
      <a href="/home" class="{{{ Request::is('home') ? 'nav-item is-tab is-active' : 'nav-item is-tab' }}}">Home</a>
      <a href="/budgets" class="{{{ Request::is('budgets') ? 'nav-item is-tab is-active' : 'nav-item is-tab' }}}">Budgets</a>
      <a class="nav-item is-tab" href="{{ url('/logout') }}"
         onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
         Logout
     </a>

     <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
         {{ csrf_field() }}
     </form>
     @endif
    </div>
  </div>
</nav>
     </div>