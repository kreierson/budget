<div class="modal" v-bind:class="{'is-active': modalActive}">
  <div class="modal-background"></div>
  <div class="modal-card">
 <form action="/budgets" method="post">
 {{csrf_field()}}
    <header class="modal-card-head">
      <p class="modal-card-title">Create a new budget</p>
      <button class="delete" @click="toggleCreateBudgetModal"></button>
    </header>
    <section class="modal-card-body">
      <label>Budget Name</label>
     <p class="control">
       <input class="input is-medium" name="name" type="text" placeholder="Enter the name of your new budget">
     </p>
     <div>
          <label>Budget Start Day</label>
          <p class="control">
               <span class="select">
                    <select name="start_day" v-model="startDay">
                         <option value="">-- Choose Day --</option>
                         <option>Sunday</option>
                         <option>Monday</option>
                         <option>Tuesday</option>
                         <option>Wednesday</option>
                         <option>Thursday</option>
                         <option>Friday</option>
                         <option>Saturday</option>
                    </select>
               </span>
          </p>
     </div>
     @if(Auth::user()->budgets->count() > 0)
     <p class="control">
          <input type="checkbox" name="default_budget">
          <label>Default Budget</label>
     </p>
     @endif
    </section>
    <footer class="modal-card-foot">
      <button class="button is-primary">Save changes</button>
      <a class="button" @click="toggleCreateBudgetModal">Cancel</a>
    </footer>
</form>
  </div>
</div>