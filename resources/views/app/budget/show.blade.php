@extends('layouts.app')

@section('content')
<div id="app">
@include('app.partials.invite-user-modal')
<div class="box">
      <div class="heading">
          <div class="columns">
               <div class="column is-6">
                     <h1 class="title">@{{budget.name}}</h1>
                     <h2 class="subtitle">@{{budget.type}} budget starting every @{{budget.default_day}}</h2>
               </div>
               <div class="column has-text-right">
                    @if(Auth::user()->default_budget_id != $budget->id)
                         <form action="/budgets/{{$budget->id}}/make-default" method='post' style="display: inline;">
                              {{csrf_field()}}
                              <button class="button is-primary is-outlined">Make Default</button>
                         </form>
                    @endif
                    <form action="/budgets/{{$budget->id}}/add-period" method="post" style="display: inline;">
                         {{csrf_field()}}
                         <a class="button is-primary is-outlined" @click="toggleInviteUserModal">Invite</a>
                         <button class="button is-primary" style="display: inline;">Add Next Week</button>
                    </form>
               </div>
          </div>
      </div>
     <div class="columns is-multiline">
          <div class="column is-6" v-for="period in timePeriods">
               <time-period :period="period"></time-period>
          </div>
     </div>
</div>
</div>

<script id="timePeriod" type="text/x-template">
          
          {{-- This card needs to be it's own component, then pass it the period as a prop --}}
               <div class="card">  
                              <h1 class="title">@{{period.start}} - @{{period.end}}</h1>
                              <h2 class="subtitle">This week's budget: $@{{sumStuff(items)}}</h2>
                         <hr>
                    <div class="card-content">
                         <div class="content">
                              <table class="table">
                                   <thead>
                                        <th>Budget Item</th>
                                        <th>Amount</th>
                                        <th>Spent</th>
                                        <th>Left</th>
                                        <th v-show="editMode"><a class="button" @click="addItem(period.id)">Add</a></th>
                                   </thead>
                                   <tbody>
                                   {{-- I'm assuming this will need to be it's own component, then pass it an item as a prop --}}
                                        <tr v-for="item in items" v-on:keydown.enter="saveWeek">
                                             <td><span v-show="!editMode">@{{item.item_description}}</span><span v-show="editMode"><input class="input" type="text" placeholder="Item Description" v-model="item.item_description"></span></td>
                                             <td><span v-show="!editMode">$@{{item.amount}}</span><span v-show="editMode"><input class="input" type="text" placeholder="Amount" v-model="item.amount" v-on:keydown.tab="addItem(period.id)"></span></td>
                                             <td>$@{{sumStuff(item.transactions)}}</td>
                                             <td>$@{{item.amount - sumStuff(item.transactions)}}</td>
                                             <th v-show="editMode"><a class="delete" @click="removeItem(item)"></a></th>
                                        </tr>
                                   </tbody>
                                   <tfoot>
                                        <tr>
                                             <th>Total</th>
                                             <th>$@{{sumStuff(items)}}</th>
                                             <th>$@{{sumStuff(period.transactions)}}</th>
                                             <th>$@{{sumStuff(items)-sumStuff(period.transactions)}}</th>
                                             <th v-show="editMode"></th>
                                        </tr>
                                   </tfoot>
                              </table>
                         </div>
                    </div>
                    <footer class="card-footer">
                         <a class="card-footer-item" @click="saveWeek" v-show="editMode">Save</a>
                         <a class="card-footer-item" @click="toggleEditMode" v-show="!editMode">Edit</a>
                         <a class="card-footer-item" @click="toggleEditMode" v-show="editMode">Cancel Edit</a>
                         <a class="card-footer-item" @click="deleteWeek(period)" v-show="!editMode && morePeriods > 1">Delete</a>
                         {{-- <a class="card-footer-item">Delete</a> --}}
                    </footer>
               </div>
</script>
<script>
     Vue.component('time-period', {
          props: ['period'],
          template: '#timePeriod',
          data: function() {
               return {
                    editMode: false,
                    items: this.period.budget_items,
                    morePeriods: this.$parent.timePeriods.length,
               }
          },
          methods: {
               sumStuff: function(items) {
                    if(items) {
                         return items.reduce(function(sum, next) {
                               return sum + Math.round(next.amount);
                           }, 0);
                    } else {
                         return 0;
                    }
               },
               addItem: function(timePeriodId) {
                    this.period.budget_items.push({"item_description":null,"amount":0,"time_period_id":timePeriodId,"id":null});
                    // axios.post('/time-period/'+timePeriodId+'/budget-item').then(response => this.items.push(response.data));
               },
               removeItem: function(item) {
                         var index = this.period.budget_items.indexOf(item);
                    if(item.id) {
                         axios.delete('/budget-item/'+item.id).then(response =>  {
                              this.period.budget_items.splice(index, 1);
                         });
                    } else {
                         this.period.budget_items.splice(index, 1);
                    }
               },
               saveWeek: function() {
                    if(this.editMode) {
                         self = this;
                         axios.post('/budget-item/save-week/'+this.period.id, this.period.budget_items).then(response => {
                              this.period.budget_items = response.data;
                              this.toggleEditMode();
                         });
                    } else {
                         alert('You must be in edit mode to save');
                    }
               },
               toggleEditMode: function() {
                    this.editMode = !this.editMode;
               },
               deleteWeek: function(period) {
                    axios.delete('/time-period/'+period.id).then(response => {
                         var index = this.$parent.timePeriods.indexOf(period);
                         this.$parent.timePeriods.splice(index, 1);
                    });
               },
          }
     });
     new Vue({
          el: '#app',
          data: {
               budget: {!!$budget!!},
               timePeriods: {!!$timePeriods!!},
               modalActive: false,
          },
          methods: {
               addWeek: function() {
                     // axios.post('/budget/'+this.budget.id+'/add-period').then(response => this.timePeriods.unshift(response.data));
                     axios.post('/budget/'+this.budget.id+'/add-period').then(response => console.log(response.data));
               },
               toggleInviteUserModal: function() {
                    this.modalActive = !this.modalActive;
               }
          }
     });
</script>
@endsection