@extends('layouts.app')

@section('content')
<div id="app">

@include('app.partials.create-budget-modal')


<div class="box">
 <div class="heading">
     <div class="columns">
     <div class="column is-three-quarters">
        <h1 class="title">My Budgets</h1>
     </div>
     <div class="column has-text-right">
          <a class="button is-primary" @click="toggleCreateBudgetModal">Create Budget</a>
     </div>
      </div>
      </div>
<div class="columns is-multiline">
@if($budgets->count())
@foreach($budgets as $budget)
<div class="column is-4">
 <div class="card">
 @if(Auth::user()->default_budget_id == $budget->id)
     <span class="icon is-pulled-right">
       <i class="fa fa-check-circle is-default-budget"></i>
     </span>
@endif
<div class="column has-text-centered">
<h1 class="title">{{$budget->name}}</h1>
<h2 class="subtitle">Restarts every {{$budget->default_day}}</h2>
</div>
<form action="/budgets/{{$budget->id}}" method="post">
{{csrf_field()}}
<input name="_method" type="hidden" value="DELETE">
<footer class="card-footer">
          <a href="/budgets/{{$budget->id}}" class="card-footer-item">Edit</a>
          <button class="card-footer-item" style="background-color: white; border: none; "><a class="card-footer-item" >Delete</a></button>
</footer>
</form>
</div>
</div>
@endforeach
@else
<div class="columns">
     <div class="column">
          <h2 class="subtitle">You currently do not have any budgets created. Create one by clicking the button above.</h2>
     </div>
</div>
@endif
</div>
</div>
</div>


<script>
new Vue({
  el: '#app',
  data: {
          modalActive: false,
          startDay: '',
          hovering: false,
  },
  methods: {
     toggleCreateBudgetModal: function() {
          this.modalActive = !this.modalActive;
     }
  },
});

</script>
@endsection