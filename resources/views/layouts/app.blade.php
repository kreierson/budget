<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>The Weekly Budget</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.3.1/css/bulma.css" rel="stylesheet">

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.3/axios.js"></script>
</head>
<body>
@include('app.partials.nav')
<section class="section">
<div class="container">
        @if ( $errors->any() )
          <div class="notification is-danger">
            {{-- <button class="delete"></button> --}}
            <ul>
                 @foreach ( $errors->all() as $error )
                 <li>{{ $error }}</li>
                 @endforeach
             </ul>
          </div>
          @endif

        @yield('content')
</div>
</section>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>

<script>
     new Vue({
          el: '#nav',
          data: {
               navActive: false,
          },
          methods: {
               toggleNav: function() {
                    this.navActive = !this.navActive;
               }
          }
     });
</script>
