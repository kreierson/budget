@extends('layouts.app')

@section('content')
<div class="columns">
     <div class="column is-6 is-offset-3">
<div class="card">
<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
     <div class="card-content">
          <div class="content">
                        {{ csrf_field() }}
                              <label for="name" class="label">Name</label>
                            <p class="control">
                                <input id="name" type="name" class="input" name="name" value="{{ old('name') }}" required autofocus>
                              </p>
                            <label for="email" class="label">E-Mail Address</label>
                            <p class="control">
                                <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required autofocus>
                              </p>
                            <label for="password" class="label">Password</label>
                            <p class="control">
                              <input id="password" type="password" class="input" name="password" required>
                              </p>
                         <label for="password" class="label">Confirm Password</label>
                            <p class="control">
                              <input id="password" type="password" class="input" name="password_confirmation" required>
                              </p>

               <button type="submit" class="button is-primary is-outlined">Register</button>
                                
          </div>
     </div>
          
</form>
</div>
</div>
</div>
@endsection
