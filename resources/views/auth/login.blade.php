@extends('layouts.app')

@section('content')
<div class="columns">
     <div class="column is-6 is-offset-3">
<div class="card">
<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
     <div class="card-content">
          <div class="content">
                        {{ csrf_field() }}
                            <label for="email" class="label">E-Mail Address</label>
                            <p class="control">
                                <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required autofocus>
                              </p>
                            <label for="password" class="label">Password</label>
                            <p class="control">
                              <input id="password" type="password" class="input" name="password" required>
                              </p>

                        <p class="control">
                           <label class="checkbox">
                             <input type="checkbox" name="remember">
                             Remember me
                           </label>
                           <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                         </p>
               <button type="submit" class="button is-primary is-outlined">Login</button>
                                
          </div>
     </div>
          
</form>
</div>
</div>
</div>
@endsection
