<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('time_period_id')->unsigned();
            $table->integer('budget_id')->unsigned();
            $table->integer('budget_item_id')->unsigned();
            $table->decimal('amount', 13, 2);
            $table->timestamps();

              $table->foreign('time_period_id')
               ->references('id')
               ->on('time_periods')
               ->onDelete('cascade');

              $table->foreign('budget_id')
               ->references('id')
               ->on('budgets')
               ->onDelete('cascade');

              $table->foreign('budget_item_id')
               ->references('id')
               ->on('budget_items')
               ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
